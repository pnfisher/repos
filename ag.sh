#!/bin/bash

. util.sh
[ "$1" != "noprompt" ] && prepprompt

optdir=${HOME}/opt
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)
ldir=the_silver_searcher
git=https://github.com/ggreer/the_silver_searcher.git
ver=0.32.0

rm -rf ${ldir}
echo "git cloning of ${git}"
git clone ${git}
if (($? != 0)); then
  echo "error, git clone of ${git} failed" 1>&2
  exit 1
fi

(
  set -ex
  cd ${ldir}
  git checkout ${ver}
  ./build.sh --prefix=${tdir}
)
if [ $? != 0 ]; then
  echo "error: ${ldir} build failed"
  exit 1
fi

(
  set -ex
  cd ${ldir}
  make install
)
if [ $? != 0 ]; then
  echo "error: ${ldir} install failed"
  exit 1
fi

echo "cleaning up ${ldir} with make clean and git clean -f"
(cd ${ldir}; make clean >& /dev/null; git clean -f)
