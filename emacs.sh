#!/bin/bash

# you may need to do this first:
#
#   sudo apt-get build-dep emacs23 or emacs24 etc.
# or
#   sudo dnf builddep emacs
#

. util.sh
[ "$1" != "noprompt" ] && prepprompt

ver=25.3
optdir=${HOME}/opt
ldir=emacs-${ver}
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)/emacs
http=http://ftp.gnu.org/gnu/emacs
tar=emacs-${ver}.tar.xz

rm -rf ${ldir}
curl -sS ${http}/${tar} | tar Jxvf -
if (($? != 0)); then
  rm -rf ${ldir}
  echo "error, download of ${http} failed" 1>&2
  exit 1
fi

if [ ! -d ${ldir} ]; then
  echo "error: couldn't find directory '${ldir}'" 1>&2
  exit 1
fi

(
  set -ex
  cd ${ldir}
  # if [ "$DISTRO" = "Fedora" ]; then
  #   (cd lisp/progmodes && patch < ../../../emacs.patch)
  # elif [ "$DISTRO" = "Ubuntu" ] && [ "$DISTRIB_RELEASE" = "16.04" ]; then
  #   (cd lisp/progmodes && patch < ../../../emacs.patch)
  # fi
  ./autogen.sh
  if [ "$DISTRO" = "CentOS" ]; then
    ./configure --with-x-toolkit=lucid \
                --prefix=${tdir}/${ver} \
                --exec-prefix=${tdir}/${ver} \
                --with-jpeg=no \
                --with-gif=no \
                --with-tiff=no \
                --with-png=no
  else
    ./configure --with-x-toolkit=lucid \
                --prefix=${tdir}/${ver} \
                --exec-prefix=${tdir}/${ver}
  fi
  make clean
  make -j4
)
if [ $? != 0 ]; then
  echo "error: emacs build failed" 1>&2
  exit 1
fi

(
  set -ex
  mkdir -p ${tdir}
  rm -rf ${tdir}/*
  cd ${ldir}
  make install
)
if [ $? != 0 ]; then
  echo "error: emacs install failed" 1>&2
  exit 1
fi

(cd ${ldir}; make clean)
