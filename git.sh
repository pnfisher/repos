#!/bin/bash

. util.sh
[ "$1" != "noprompt" ] && prepprompt

#ver=2.16.1
#http=https://www.kernel.org/pub/software/scm/git
ver=2.18.0
http=https://git.kernel.org/pub/scm/git/git.git/snapshot
tar=git-${ver}.tar.gz
optdir=${HOME}/opt
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)
ldir=git-${ver}

rm -rf ${ldir}
curl -sS ${http}/${tar} | tar zxvf -
if (($? != 0)); then
  rm -rf ${ldir}
  echo "error, download of ${http} failed" 1>&2
  exit 1
fi

if [ ! -d ${ldir} ]; then
  echo "error: couldn't find directory ${ldir}"
  exit 1
fi

(
  cd ${ldir}
  make configure
  ./configure --prefix=${tdir} --with-curl --with-expat
  make all
  if [ "$DISTRO" != "CentOS" ] && [ "$DISTRO" != "Cygwin" ]; then
    make doc
  fi
)
if [ $? != 0 ]; then
  echo "error: ${ldir} build failed"
  exit 1
fi

(
  cd ${ldir}
  make install
  if [ "$DISTRO" != "CentOS" ] && [ "$DISTRO" != "Cygwin" ]; then
    make install-doc install-html
  fi
)
if [ $? != 0 ]; then
  echo "error: ${ldir} install failed"
  exit 1
fi

echo "cleaning up ${ldir} with make clean"
(cd ${ldir}; make clean >& /dev/null)
