#!/bin/bash

. util.sh
[ "$1" != "noprompt" ] && prepprompt

optdir=${HOME}/opt
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)
ldir=glimpse
git=https://github.com/pnfisher/glimpse.git

rm -rf ${ldir}
echo "git cloning of ${git}"
git clone --recursive ${git}
if (($? != 0)); then
  echo "error, git clone of ${git} failed" 1>&2
  exit 1
fi

(
  cd ${ldir}
  ./configure --prefix=${tdir}
  make
)
if [ $? != 0 ]; then
  echo "error: glimpse build failed"
  exit 1
fi

(
  cd ${ldir}
  make install
)
if [ $? != 0 ]; then
  echo "error: glimpse install failed"
  exit 1
fi

echo "cleaning up glimpse with make clean and git clean -f"
(cd ${ldir}; make clean >& /dev/null; git clean -f)
