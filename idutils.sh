#!/bin/bash

. util.sh

ver=4.6
http=http://ftp.gnu.org/gnu/idutils
tar=idutils-${ver}.tar.xz
optdir=${HOME}/opt
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)
ldir=idutils-${ver}

rm -rf ${ldir}
curl -sS ${http}/${tar} | tar Jxvf -
if (($? != 0)); then
  rm -rf ${ldir}
  echo "error, download of ${http} failed" 1>&2
  exit 1
fi

if [ ! -d ${ldir} ]; then
  echo "error: couldn't find directory ${ldir}"
  exit 1
fi

(
  cd ${ldir}
  sed -i "s/_GL_WARN_ON_USE (gets, .*//g" ./lib/stdio.in.h
  if [ "$DISTRIB_CODENAME" = "CentOS-8.1" ]; then
    sed -i 's/_IO_ftrylockfile/__USE_POSIX199506/g' lib/fseterr.c
    sed -i 's/_IO_ftrylockfile/__USE_POSIX199506/g' lib/fseeko.c
  fi
  ./configure --prefix=${tdir}
  make
)
if [ $? != 0 ]; then
  echo "error: ${ldir} build failed"
  exit 1
fi

(
  cd ${ldir}
  make install
)
if [ $? != 0 ]; then
  echo "error: ${ldir} install failed"
  exit 1
fi

[ -f ${ldir}/gtags.el -a -d ${HOME}/src/elisp ] && \
rsync -av ${ldir}/gtags.el ${HOME}/src/elisp/gtags.el

echo "removing ${ldir}"
rm -rf ${ldir}
