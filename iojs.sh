#!/bin/bash

#
# warning:
#
# if you have a
#
#   ${HOME}/.gyp/include.gypi
#
# file with
#
#  'component' : 'shared_library'
#
# set, you'll break this build. iojs must build the v8 dep statically
#

optdir=${HOME}/opt
ldir=io.js
tdir=${optdir}/${ldir}

which clang++ >& /dev/null
if [ $? != 0 ]; then
  echo "error: you need to sudo apt-get install clang 3.4 or greater"
  exit 1
fi

git submodule init
git submodule update

if [ ! -d ${ldir} ]; then
  echo "error: couldn't find directory \'${ldir}\'"
  exit 1
fi

(
  cd ${ldir}
  export CXX=clang++
  ./configure --prefix=${tdir}
  make clean
  make
)
if [ $? != 0 ]; then
  echo "error: iojs build failed"
  exit 1
fi

(
  cd ${ldir}
  mkdir -p ${tdir}
  rm -rf ${tdir}/*
  make install
)
if [ $? != 0 ]; then
  echo "error: iojs install failed"
  exit 1
fi

ln -sf ${tdir}/bin/* ${optdir}/bin/.
(cd ${ldir}; make clean)

