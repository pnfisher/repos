#!/bin/bash

. util.sh
[ "$1" != "noprompt" ] && prepprompt

rel=3.8.0
http=http://llvm.org/releases/${rel}
optdir=${HOME}/opt
ldir=llvm
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)

(
  set -ex

  rm -rf ${ldir}
  mkdir ${ldir}
  cd ${ldir}

  for f in llvm cfe compiler-rt libcxx libcxxabi clang-tools-extra openmp; do
    curl -sS ${http}/$f-${rel}.src.tar.xz | tar Jxf -
  done

  ln -s llvm-${rel}.src llvm
  cd llvm
  cd ..

  # clang
  cd llvm/tools
  ln -s ../../cfe-${rel}.src clang
  cd clang
  cd ../../..

  # clang extra tools (clang-format, clang-tidy etc.)
  cd cfe-${rel}.src/tools
  ln -s ../../clang-tools-extra-${rel}.src extra
  cd extra
  cd ../../..

  mkdir -p llvm/projects
  for f in compiler-rt openmp libcxx libcxxabi; do
    cd llvm/projects
    ln -s ../../$f-${rel}.src $f
    cd $f
    cd ../../..
  done

  mkdir -p build
)
if [ $? != 0 ]; then
  echo "error: llvm ${rel} downloads and setup failed" 1>&2;
  #rm -rf ${ldir};
  exit 1;
fi

(
  set -ex
  cd llvm/build
  cmake -G "Unix Makefiles" \
        -DCMAKE_INSTALL_PREFIX=${tdir} \
        -DLLVM_TARGETS_TO_BUILD="X86;CppBackend" \
        -DCMAKE_BUILD_TYPE=Release \
        ../llvm
  make -j4
  make install
  make clean
  rm -rf *
  CC=${tdir}/bin/clang CXX=${tdir}/bin/clang++ cmake \
    -G "Unix Makefiles" \
    -DCMAKE_INSTALL_PREFIX=${tdir} \
    -DLLVM_TARGETS_TO_BUILD="X86;CppBackend" \
    -DLLVM_BUILD_DOCS=ON \
    -DLLVM_ENABLE_SPHINX=ON \
    -DSPHINX_OUTPUT_MAN=ON \
    -DSPHINX_OUTPUT_HTML=OFF \
    -DCMAKE_BUILD_TYPE=Release \
    ../llvm
  make -j4
  make install
  make clean
)
if [ $? != 0 ]; then
  echo "error: llvm build failed" 1>&2;
  exit 1;
fi
