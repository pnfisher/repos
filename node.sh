#!/bin/bash

version=v4.2.1
arch=$(uname -m)
if [ "$arch" == "x86_64" ]; then
  tarball=node-${version}-linux-x64.tar.gz
else
  tarball=node-${version}-linux-x86.tar.gz
fi
http=https://nodejs.org/dist/${version}/${tarball}
downdir=${HOME}/Downloads/nodejs
optdir=${HOME}/opt
tdir=${optdir}/nodejs

mkdir -p ${downdir}
if [ ! -f ${downdir}/${tarball} ]; then
  echo wgetting ${http}
  (cd ${downdir} && wget ${http})
  if (($? != 0)); then
    echo "error, wget ${tarball} failed" 1>&2
    exit 1
  fi
fi

mkdir -p ${tdir}
echo removing ${tdir}/${arch}
rm -rf ${tdir}/${arch}
echo extracting ${tarball} into ${tdir}
(cd ${tdir} && \
tar zxf ${downdir}/${tarball} && \
mv $(basename ${tarball} .tar.gz) ${arch})
if (($? != 0)); then
  echo \
    "error, couldn't extract and rename ${tarball} to ${arch} in ${tdir}" 1>&2
  exit 1
fi
