#!/bin/bash

optdir=${HOME}/opt
nwdir=nwjs-v0.12.2-linux-x64
tdir=${optdir}/${nwdir}

rm -rf ${tdir}
curl -sS http://dl.nwjs.io/v0.12.2/${nwdir}.tar.gz | (cd ${optdir}; tar zxvf -)
if [ $? == 0 ]; then
  ln -sf ${tdir}/nw* ${optdir}/bin/.
fi
