#!/bin/bash

optdir=${HOME}/opt
docdir=${optdir}/python/docs
ver2=https://docs.python.org/2/archives/python-2.7.10-docs-text.tar.bz2
ver3=https://docs.python.org/3.4/archives/python-3.4.3-docs-text.tar.bz2

rm -rf ${docdir}
mkdir -p ${docdir}

for ver in ${ver2} ${ver3}; do
  curl -sS ${ver} | (cd ${docdir}; tar jxvf -)
  if [ $? != 0 ]; then
    echo "error: failed to download and extract ${ver}"
    exit 1
  fi
done
(
  v=2
  cd ${docdir}
  for d in `ls -d */`; do
		files=`find ${docdir}/${d} -type f -print`
		glimpseindex -F -H ${docdir}/${d} -E -B -n -o ${files}
    ln -sf ${d} ${v}
    v=$((v+1))
	done
)
