#!/bin/bash

. util.sh
[ "$1" != "noprompt" ] && prepprompt

optdir=${HOME}/opt
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)
ldir=rtags
git=https://github.com/Andersbakken/rtags.git
ver=v2.3

rm -rf ${ldir}
echo "git cloning of ${git}"
git clone --recursive ${git}
if (($? != 0)); then
  echo "error, git clone of ${git} failed" 1>&2
  exit 1
fi

(
  set -ex
  cd ${ldir}
  (cd src/rct && git checkout 32c4cac)
  git checkout ${ver}
  git apply ../rtags.patch
  rm -rf CMakeCache.txt
  cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
        -DCMAKE_INSTALL_PREFIX=${tdir} \
        -DCMAKE_CXX_COMPILER=$(which clang++) \
        -DCMAKE_C_COMPILER=$(which clang) \
        .
  make -j4
)
if [ $? != 0 ]; then
  echo "error: rtags build failed"
  exit 1
fi

(
  set -ex
  cd ${ldir}
  make install
)
if [ $? != 0 ]; then
  echo "error: rtags install failed"
  exit 1
fi

echo "cleaning up rtags with make clean and git clean -f"
(cd ${ldir}; make clean >& /dev/null; git clean -f)
