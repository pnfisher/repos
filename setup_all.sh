#!/bin/bash

pname=$(basename $0)

. util.sh || exit 1
prepprompt

set -x

./ag.sh noprompt || exit 1
## glimpse before pythondocs, since pythondocs depends upon glimpse
./glimpse.sh noprompt || exit 1
# ./pythondocs.sh || exit 1
./global.sh noprompt || exit 1
./idutils.sh noprompt || exit 1
./git.sh noprompt || exit 1
./emacs.sh noprompt || exit 1
## llvm takes a long time (but rtags needs it)
./llvm.sh noprompt || exit 1
## rtags after llvm so we can use clang++
./rtags.sh noprompt || exit 1
## retired for now
#./iojs.sh || exit 1
#./node.sh || exit 1
#./nwjs.sh || exit 1

