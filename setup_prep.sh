#!/bin/bash

pname=$(basename $0)

if [ $(whoami) != "root" ]; then
  echo "error, please run using sudo" 1>&2
  exit 1
fi

. util.sh || exit 1

packages="flex curl cmake clang python-sphinx"
builddeps="curl llvm flex git clang global subversion"

if [ "$DISTRO" = "Ubuntu" ]; then
  # packages+=" libncurses5"
  packages+=" xaw3dg-dev"
  builddeps+=" emacs24 silversearcher-ag"
elif [ "$DISTRO" = "Fedora" ]; then
  # packages+=" ncurses-devel"
  packages+=" emacs Xaw3d-devel perl-Net-*"
  builddeps+=" emacs"
else
  echo "error, unsupported Linux distribution"
  exit 1
fi

for p in ${packages}; do
  echo ${pname}: doing install $p
  case $DISTRO in
    Ubuntu) apt-get -y -q install $p;;
    Fedora) dnf -y -q install "$p";;
    *) exit 1;;
  esac
done

for p in ${builddeps}; do
  echo ${pname}: doing build dep $p
  case $DISTRO in
    Ubuntu) apt-get -y -q build-dep $p;;
    Fedora) dnf -y -q builddep $p;;
    *) exit 1;;
  esac
done
