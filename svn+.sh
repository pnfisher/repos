#!/bin/bash

. util.sh
[ "$1" != "noprompt" ] && prepprompt

ver=1.5.2
http=http://apache.mirror.iweb.ca//apr
tar=apr-${ver}.tar.gz
optdir=${HOME}/opt
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)
ldir=apr-${ver}

rm -rf ${ldir}
curl -sS ${http}/${tar} | tar zxvf -
if (($? != 0)); then
  rm -rf ${ldir}
  echo "error, download of ${http} failed" 1>&2
  exit 1
fi

if [ ! -d ${ldir} ]; then
  echo "error: couldn't find directory ${ldir}"
  exit 1
fi

(
  cd ${ldir}
  #./autogen.sh
  ./configure --prefix=${tdir}
  make -j4
)
if [ $? != 0 ]; then
  echo "error: ${ldir} build failed"
  exit 1
fi

(
  cd ${ldir}
  make install
)
if [ $? != 0 ]; then
  echo "error: ${ldir} install failed"
  exit 1
fi

echo "cleaning up ${ldir} with make clean"
(cd ${ldir}; make clean >& /dev/null)

ver=1.5.4
http=http://apache.mirror.iweb.ca//apr
tar=apr-util-${ver}.tar.gz
optdir=${HOME}/opt
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)
ldir=apr-util-${ver}

rm -rf ${ldir}
curl -sS ${http}/${tar} | tar zxvf -
if (($? != 0)); then
  rm -rf ${ldir}
  echo "error, download of ${http} failed" 1>&2
  exit 1
fi

if [ ! -d ${ldir} ]; then
  echo "error: couldn't find directory ${ldir}"
  exit 1
fi

(
  cd ${ldir}
  #./autogen.sh
  ./configure \
    --prefix=${tdir} \
    --with-apr=${tdir}
  make -j4
)
if [ $? != 0 ]; then
  echo "error: ${ldir} build failed"
  exit 1
fi

(
  cd ${ldir}
  make install
)
if [ $? != 0 ]; then
  echo "error: ${ldir} install failed"
  exit 1
fi

ver=3150200
http=https://sqlite.org/2016
tar=sqlite-autoconf-${ver}.tar.gz
optdir=${HOME}/opt
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)
ldir=sqlite-autoconf-${ver}

rm -rf ${ldir}
curl -sS ${http}/${tar} | tar zxvf -
if (($? != 0)); then
  rm -rf ${ldir}
  echo "error, download of ${http} failed" 1>&2
  exit 1
fi

if [ ! -d ${ldir} ]; then
  echo "error: couldn't find directory ${ldir}"
  exit 1
fi

(
  cd ${ldir}
  #./autogen.sh
  ./configure --prefix=${tdir}
  make -j4
)
if [ $? != 0 ]; then
  echo "error: ${ldir} build failed"
  exit 1
fi

(
  cd ${ldir}
  make install
)
if [ $? != 0 ]; then
  echo "error: ${ldir} install failed"
  exit 1
fi

## serf

set -x
ver=1.3.9
http=https://archive.apache.org/dist/serf
tar=serf-${ver}.tar.bz2
optdir=${HOME}/opt
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)
ldir=serf-${ver}

rm -rf ${ldir}
curl -sSL ${http}/${tar} | tar jxvf -
if (($? != 0)); then
  rm -rf ${ldir}
  echo "error, download of ${http} failed" 1>&2
  exit 1
fi

if [ ! -d ${ldir} ]; then
  echo "error: couldn't find directory ${ldir}"
  exit 1
fi

(
  cd ${ldir}
  ver2=2.3.0
  http2=http://prdownloads.sourceforge.net/scons
  tar2=scons-local-${ver2}.tar.gz
  curl -sSL ${http2}/${tar2} | tar zxvf -
  if (($? != 0)); then
    echo "error: couldn't get ${http2}" 1>&2
    exit 1
  fi
  ln -sf scons.py scons
  ./scons APR=${tdir} APU=${tdir} PREFIX=${tdir}
  if (($? != 0)); then
    echo "error: ${ldir} build failed" 1>&2
    exit 1
  fi
  ./scons PREFIX=${tdir} install
  if (($? != 0)); then
    echo "error: ${ldir} install failed" 1>&2
    exit 1
  fi  
)
if [ $? != 0 ]; then
  echo "error: ${ldir} build failed"
  exit 1
fi

## subversion

ver=1.9.5
http=http://httpd-mirror.frgl.pw/apache/subversion
tar=subversion-${ver}.tar.gz
optdir=${HOME}/opt
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)
ldir=subversion-${ver}

rm -rf ${ldir}
curl -sS ${http}/${tar} | tar zxvf -
if (($? != 0)); then
  rm -rf ${ldir}
  echo "error, download of ${http} failed" 1>&2
  exit 1
fi

if [ ! -d ${ldir} ]; then
  echo "error: couldn't find directory ${ldir}"
  exit 1
fi

(
  cd ${ldir}
  # ./autogen.sh
  ./configure \
    --prefix=${tdir} \
    --with-apr=${tdir} \
    --with-apr-util=${tdir} \
    --with-sqlite=${tdir} \
    --with-serf=${tdir}
  make -j4
)
if [ $? != 0 ]; then
  echo "error: ${ldir} build failed"
  exit 1
fi

(
  cd ${ldir}
  make install
)
if [ $? != 0 ]; then
  echo "error: ${ldir} install failed"
  exit 1
fi

echo "cleaning up ${ldir} with make clean"
(cd ${ldir}; make clean >& /dev/null)
