ynprompt () {
  while true; do
    read -p "$1" yn
    case $yn in
      [Yy]*) break;;
      [Nn]*) exit;;
      *) echo "Please answer y or n.";;
    esac
  done
}

prepprompt () {
  ynprompt "Have you run sudo ./setup_prep.sh (y/n)? "
}

DISTRO=
if [ -f /etc/centos-release ]; then
  cat /etc/centos-release | grep "^CentOS release 6.5" >& /dev/null
  if [ $? = 0 ]; then
    DISTRIB_CODENAME="CentOS-6.5"
    DISTRO="CentOS"
  fi
  cat /etc/centos-release | grep "^CentOS Linux release 8.1" >& /dev/null
  if [ $? = 0 ]; then
    DISTRIB_CODENAME="CentOS-8.1"
    DISTRO="CentOS"
  fi
elif [ -f /etc/lsb-release ]; then
  . /etc/lsb-release
  DISTRO=${DISTRIB_ID}
elif [ -f /etc/fedora-release ]; then
  echo "couldn't source /etc/lsb-release, trying /etc/os-release" 1>&2
  . /etc/os-release || {
    echo "error, couldn't source /etc/os-release" 1>&2;
    exit 1;
  }
  DISTRIB_CODENAME="${ID}${VERSION_ID}"
  DISTRO=${NAME}
elif [ -f /etc/cygport.conf ]; then
  DISTRIB_CODENAME="CYGWIN_NT-10.0"
  DISTRO=Cygwin
fi

if [ -z "$DISTRIB_CODENAME" ]; then
  echo "error, couldn't figure out DISTRIB_CODENAME" 1>&2
  exit 1
fi

if [ -z "$DISTRO" ]; then
  echo "error, couldn't figure out DISTRO" 1>&2
  exit 1
fi
