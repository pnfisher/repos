#!/bin/bash

. util.sh
[ "$1" != "noprompt" ] && prepprompt

ver=1.07
http=http://ftp.gnu.org/gnu/uucp
tar=uucp-${ver}.tar.gz
optdir=${HOME}/opt
tdir=${optdir}/${DISTRIB_CODENAME}/$(uname -m)
ldir=uucp-${ver}

rm -rf ${ldir}
curl -sS ${http}/${tar} | tar zxvf -
if (($? != 0)); then
  rm -rf ${ldir}
  echo "error, download of ${http} failed" 1>&2
  exit 1
fi

if [ ! -d ${ldir} ]; then
  echo "error: couldn't find directory ${ldir}"
  exit 1
fi

(
  cd ${ldir}
  ./configure --prefix=${tdir}
  make
)
if [ $? != 0 ]; then
  echo "error: ${ldir} build failed"
  exit 1
fi

(
  cd ${ldir}
  make install
)
if [ $? != 0 ]; then
  echo "error: ${ldir} install failed"
  exit 1
fi

echo "cleaning up ${ldir} with make clean"
(cd ${ldir}; make clean >& /dev/null)
